class Manager extends Employee {

    //    Attributes
    private String managementName;

//    Constructor
    Manager(String name, int age, char sex, double salary,  String registration){
        setName(name);
        setAge(age);
        setSex(sex);
        setSalary(salary);
        setRegistration(registration);
    }

    //    Accessors and Modifiers Methods (Getters and Setters)
    public String getManagementName() {
        return managementName;
    }

    public void setManagementName(String managementName) {
        this.managementName = managementName;
    }

    //    Others Methods
    @Override
    public String toString() {
        return "Manager{" +
                "name=" + getName() +
                ", age=" + getAge() +
                ", sex=" + getSex() +
                ", registration=" + getRegistration() +
                ", INSS value=" + inssValue() +
                ", managementName='" + this.getManagementName() + '\'' +
                '}';
    }
}
