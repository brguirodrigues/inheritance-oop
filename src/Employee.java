class Employee extends Person{

//    Attributes
    private double salary;
    private String registration;

//    Methods
    public double inssValue(){
        double inss;
//        Origin: http://www.calculoexato.net/como-calcular-inss/
        if (this.getSalary()<=1693.72)
            inss = this.getSalary()*0.08;
        else if(this.getSalary()>=1693.73 && this.getSalary()<=2822.9)
            inss = this.getSalary()*0.09;
        else
            inss = this.getSalary()*0.11;
        return  inss;
    }

//    Accessors and Modifiers Methods (Getters and Setters)
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

}
