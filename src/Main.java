public class Main {
    public static void main(String[] args) {

//        Salesman
        Salesman vendedor = new Salesman("John", 32, 'R', 2000, "a123");
        System.out.println(vendedor.toString());
//        Manager
        Manager gerente = new Manager("SR K", 40, 'M', 1000000, "a789");
        System.out.println(gerente.toString());
//        Client
        Client cliente = new Client("Azaghal", 42, 'M', 1976);
        System.out.println(cliente.toString());

    }
}
