public class Person {

    //    Attributes
    private String name;
    private int age;
    private char sex;

    //    Accessors and Modifiers Methods (Getters and Setters)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        sex = Character.toUpperCase(sex);
        if (sex == 'M' || sex == 'F')
            this.sex = sex;
        else this.sex = '#';
    }


}
