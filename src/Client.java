public class Client extends Person {

//    Attributes
    private double debt;
    private int bornYear;

//    Constructor
    Client(String name, int age, char sex, int bornYear){
//        redundant (age && bornYear) ...but...
        setName(name);
        setAge(age);
        setSex(sex);
        setBornYear(bornYear);
    }

//    Accessors and Modifiers Methods

    private double getDebt() {
        return debt;
    }

    private void setDebt(double debt) {
        this.debt = debt;
    }

    private int getBornYear() {
        return bornYear;
    }

    private void setBornYear(int bornYear) {
        this.bornYear = bornYear;
    }

//    Others Methods

    @Override
    public String toString() {
        return "Client{" +
                "name=" + this.getName() +
                ", age=" + this.getAge() +
                ", sex=" + this.getSex() +
                ", debt=" + this.getDebt() +
                ", bornYear=" + getBornYear() +
                '}';
    }

}
