class Salesman extends Employee {

    //    Attributes
    private double salesValue;
    private int salesAmount;

//    Constructor
    Salesman(String name, int age, char sex, double salary, String registration){
        setName(name);
        setAge(age);
        setSex(sex);
        setSalary(salary);
        setRegistration(registration);
    }

    //    Accessors and Modifiers Methods (Getters and Setters)
    private double getSalesValue() {
        return salesValue;
    }

    private void setSalesValue(double salesValue) {
        this.salesValue = salesValue;
    }

    private int getSalesAmount() {
        return salesAmount;
    }

    private void setSalesAmount(int salesAmount) {
        this.salesAmount = salesAmount;
    }

    //    Others Methods
    @Override
    public String toString() {
        return "Salesman{" +
                "name=" + getName() +
                ", age=" + getAge() +
                ", sex=" + getSex() +
                ", salary=" + getSalary() +
                ", INSS value" + inssValue() +
                ", registration=" + getRegistration() +
                ", salesValue=" + getSalesValue() +
                ", salesAmount=" + getSalesAmount() +
                '}';
    }
}
